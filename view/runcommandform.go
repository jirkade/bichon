// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2021 Red Hat, Inc.

package view

import (
	"fmt"
	"os/exec"
	"sort"

	"github.com/gdamore/tcell/v2"
	"github.com/mattn/go-shellwords"
	log "github.com/sirupsen/logrus"

	"gitlab.com/bichon-project/bichon/model"
	"gitlab.com/bichon-project/tview"
)

type RunCommandForm struct {
	tview.Primitive

	Form     *tview.Form
	Listener RunCommandFormListener
	Error    *tview.TextView

	Command  *tview.InputField
	UseShell *tview.Checkbox

	MergeReq *model.MergeReq
	Version  *model.Series
	Patch    *model.Commit
}

type RunCommandFormListener interface {
	RunCommandFormConfirm(cmdargs, cmdenv []string)
	RunCommandFormCancel()
}

func (form *RunCommandForm) cancelFunc() {
	form.Form.SetFocus(0)
	form.Listener.RunCommandFormCancel()
	form.Error.SetText("")
	form.Command.SetText("")
}

func (form *RunCommandForm) buildCommand() ([]string, error) {
	cmdstr := form.Command.GetText()

	if form.UseShell.IsChecked() {
		cmdargs := []string{
			"/bin/sh", "-c", cmdstr,
		}

		return cmdargs, nil
	} else {
		// We're directly exec'ing, but 'shellwords' is
		// a convenient way to split a command line string
		// taking account of possible quoting around
		// whitespace.
		cmdargs, err := shellwords.Parse(cmdstr)
		if err != nil {
			return []string{}, err
		}

		path, err := exec.LookPath(cmdargs[0])
		if err != nil {
			return []string{}, err
		}
		cmdargs[0] = path
		return cmdargs, nil
	}
}

func (form *RunCommandForm) buildEnv() []string {
	env := make(map[string]string)

	if form.Patch != nil {
		env["BICHON_PATCH_HASH"] = form.Patch.Hash
		env["BICHON_PATCH_TITLE"] = form.Patch.Title
	}
	if form.Version != nil {
		env["BICHON_VERSION_INDEX"] = fmt.Sprintf("%d", form.Version.Index)
		env["BICHON_VERSION_BASE_HASH"] = form.Version.BaseHash
		env["BICHON_VERSION_START_HASH"] = form.Version.StartHash
		env["BICHON_VERSION_HEAD_HASH"] = form.Version.HeadHash
		env["BICHON_VERSION_COMMITS"] = fmt.Sprintf("%d", len(form.Version.Patches))
	}

	env["BICHON_MERGEREQ_ID"] = fmt.Sprintf("%d", form.MergeReq.ID)
	env["BICHON_MERGEREQ_VERSIONS"] = fmt.Sprintf("%d", len(form.MergeReq.Versions))
	env["BICHON_MERGEREQ_SUBMITTER_REALNAME"] = form.MergeReq.Submitter.RealName
	env["BICHON_MERGEREQ_SUBMITTER_USERNAME"] = form.MergeReq.Submitter.UserName
	env["BICHON_MERGEREQ_REF"] = form.MergeReq.OriginRef()

	env["BICHON_REPOSITORY_NICKNAME"] = form.MergeReq.Repo.NickName
	env["BICHON_REPOSITORY_DIRECTORY"] = form.MergeReq.Repo.Directory
	env["BICHON_REPOSITORY_REMOTE"] = form.MergeReq.Repo.Remote
	env["BICHON_REPOSITORY_SERVER"] = form.MergeReq.Repo.Server
	env["BICHON_REPOSITORY_PROJECT"] = form.MergeReq.Repo.Project

	var envstr []string
	for key, val := range env {
		envstr = append(envstr, fmt.Sprintf("%s=%s", key, val))
	}
	sort.Strings(envstr)

	return envstr
}

func (form *RunCommandForm) confirmFunc() {
	form.Form.SetFocus(0)

	cmdargs, err := form.buildCommand()
	cmdenv := form.buildEnv()

	if err == nil {
		form.Error.SetText("")
		form.Listener.RunCommandFormConfirm(cmdargs, cmdenv)
	} else {
		form.Error.SetText("[red::]" + err.Error() + "[::]")
	}
}

func NewRunCommandForm(listener RunCommandFormListener) *RunCommandForm {

	form := &RunCommandForm{
		Form:     tview.NewForm(),
		Error:    tview.NewTextView(),
		Listener: listener,
	}

	form.Error.SetDynamicColors(true)

	layout := tview.NewFlex()
	layout.SetBackgroundColor(tview.Styles.PrimitiveBackgroundColor)
	layout.SetBorder(true)
	layout.SetTitle("Run command")
	layout.SetDirection(tview.FlexRow)
	layout.AddItem(form.Form, 7, 0, true)
	layout.AddItem(form.Error, 3, 0, false)
	form.Primitive = Modal(layout, 68, 12)

	form.Form.SetCancelFunc(form.cancelFunc)

	form.Command = addInputField(form.Form, "Command", "", 60, false)
	form.Command.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		if event.Key() == tcell.KeyEnter {
			form.confirmFunc()
			return nil
		}
		return event
	})
	form.UseShell = addCheckbox(form.Form, "", "Run as shell command string", false, false)

	form.Form.AddDefaultButton("Execute", form.confirmFunc)
	form.Form.AddButton("Cancel", form.cancelFunc)

	return form
}

func (form *RunCommandForm) GetName() string {
	return "run-command-form"
}

func (form *RunCommandForm) SetContext(mreq *model.MergeReq, ver *model.Series, patch *model.Commit) {
	form.MergeReq = mreq
	form.Version = ver
	form.Patch = patch

	log.Infof("Context mergereq %s", mreq.String())
	if ver != nil {
		log.Infof("Context version %d", ver.Index)
	}
	if patch != nil {
		log.Infof("Context patch %s: '%s'", patch.Hash, patch.Title)
	}

}
