// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package view

import (
	"sort"

	"github.com/gdamore/tcell/v2"
	log "github.com/sirupsen/logrus"

	"gitlab.com/bichon-project/bichon/model"
	"gitlab.com/bichon-project/tview"
)

type ReportsPageListener interface {
	ReportsPageQuit()
	ReportsPageSetDefault(report model.Report)
	ReportsPageApplyReport(report model.Report)
	ReportsPageDeleteReport(report model.Report)
	ReportsPageEditReport(report model.Report)
	ReportsPageAddReport()
}

type ReportsPage struct {
	*tview.Frame
	ActionMap

	Application *tview.Application
	Listener    ReportsPageListener
	Reports     *tview.Table
}

func NewReportsPage(app *tview.Application, listener ReportsPageListener) *ReportsPage {
	reports := tview.NewTable().
		SetSelectable(true, false).
		SetSelectedStyle(tcell.StyleDefault.
			Foreground(GetStyleColor(ELEMENT_REPORTS_ACTIVE_TEXT)).
			Background(GetStyleColor(ELEMENT_REPORTS_ACTIVE_FILL)).
			Attributes(GetStyleAttrMask(ELEMENT_REPORTS_ACTIVE_ATTR)))

	layout := tview.NewFrame(reports).
		SetBorders(0, 0, 0, 0, 0, 0)

	page := &ReportsPage{
		Frame:     layout,
		ActionMap: NewActionHandler("reports-page", nil),

		Application: app,
		Listener:    listener,
		Reports:     reports,
	}

	page.registerActions()

	return page
}

func (page *ReportsPage) GetName() string {
	return "reports"
}

func (page *ReportsPage) GetKeyShortcuts() string {
	return page.ActionMap.FormatSummary(
		"quit",
		"apply-report",
		"add-report",
		"edit-report",
		"delete-report",
		"set-default-report")
}

func (page *ReportsPage) Refresh(app *tview.Application, reports model.Reports) {
	go app.QueueUpdateDraw(func() {
		page.refreshMain(app, reports)
	})
}

func (page *ReportsPage) buildReportRow(report *model.Report) [3]*tview.TableCell {
	activeStr := ""
	if report.Active {
		activeStr = "<active>"
	}
	defaultStr := ""
	if report.Default {
		defaultStr = "<default>"
	}
	return [3]*tview.TableCell{
		&tview.TableCell{
			Text:            tview.Escape(report.Name),
			Reference:       report,
			Color:           GetStyleColor(ELEMENT_REPORTS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_REPORTS_INACTIVE_FILL),
			Align:           tview.AlignLeft,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            tview.Escape(activeStr),
			Color:           GetStyleColor(ELEMENT_REPORTS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_REPORTS_INACTIVE_FILL),
			Align:           tview.AlignLeft,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            tview.Escape(defaultStr),
			Color:           GetStyleColor(ELEMENT_REPORTS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_REPORTS_INACTIVE_FILL),
			Align:           tview.AlignLeft,
			NotSelectable:   false,
		},
	}
}

func (page *ReportsPage) refreshMain(app *tview.Application, reports model.Reports) {
	page.Reports.Clear()

	sort.Sort(reports)

	for idx, _ := range reports {
		report := &reports[idx]
		row := page.buildReportRow(report)

		for col, val := range row {
			page.Reports.SetCell(idx, col, val)
		}
	}
}

func (page *ReportsPage) Activate() {
	page.Application.SetFocus(page.Reports)
}

func (page *ReportsPage) getSelectedReport() *model.Report {
	row, _ := page.Reports.GetSelection()
	return page.getReportForRow(row)
}

func (page *ReportsPage) getReportForRow(row int) *model.Report {
	if page.Reports.GetRowCount() == 0 {
		return nil
	}
	if row < 0 {
		return nil
	}

	cell := page.Reports.GetCell(row, 0)
	log.Infof("Selected %d %p", row, cell)
	ref := cell.GetReference()

	if ref == nil {
		return nil
	}

	mreq, ok := ref.(*model.Report)
	if !ok {
		return nil
	}

	return mreq
}

func (page *ReportsPage) registerActions() {
	page.ActionMap.RegisterAction(
		"quit", "Index",
		func() bool {
			page.Listener.ReportsPageQuit()
			return true
		},
		NewActionRune('q', tcell.ModNone),
		NewActionKey(tcell.KeyEscape, tcell.ModNone),
	)
	page.ActionMap.RegisterAction(
		"edit-report", "Edit report",
		func() bool {
			report := page.getSelectedReport()
			if report != nil {
				page.Listener.ReportsPageEditReport(*report)
			}
			return true
		},
		NewActionRune('e', tcell.ModNone),
	)
	page.ActionMap.RegisterAction(
		"delete-report", "Delete report",
		func() bool {
			report := page.getSelectedReport()
			if report != nil {
				page.Listener.ReportsPageDeleteReport(*report)
			}
			return true
		},
		NewActionRune('d', tcell.ModNone),
	)
	page.ActionMap.RegisterAction(
		"add-report", "Add report",
		func() bool {
			page.Listener.ReportsPageAddReport()
			return true
		},
		NewActionRune('a', tcell.ModNone),
	)
	page.ActionMap.RegisterAction(
		"set-default-report", "Set default",
		func() bool {
			report := page.getSelectedReport()
			if report != nil {
				page.Listener.ReportsPageSetDefault(*report)
			}
			return true
		},
		NewActionRune('t', tcell.ModNone),
	)
	page.ActionMap.RegisterAction(
		"apply-report", "Apply report",
		func() bool {
			report := page.getSelectedReport()
			if report != nil {
				page.Listener.ReportsPageApplyReport(*report)
			}
			return true
		},
		NewActionKey(tcell.KeyEnter, tcell.ModNone),
	)
}
