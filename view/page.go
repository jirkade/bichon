// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package view

import (
	"gitlab.com/bichon-project/tview"
)

type Form interface {
	tview.Primitive
	GetName() string
}

type Page interface {
	Form
	ActionMap
	GetKeyShortcuts() string
	Activate()
}
