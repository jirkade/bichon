// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2021 Red Hat, Inc.

package view

import (
	"fmt"
	"os"
	"path/filepath"
	"sort"
	"strings"

	"github.com/gdamore/tcell/v2"
	"github.com/go-ini/ini"
	log "github.com/sirupsen/logrus"

	"gitlab.com/bichon-project/bichon/config/xdg"
)

type KeymapEntry struct {
	Context   string
	Action    string
	Shortcuts []string
}

type Keymap struct {
	Entries map[string]KeymapEntry
}

func NewKeymap() *Keymap {
	return &Keymap{
		Entries: make(map[string]KeymapEntry),
	}
}

func actionKeyToName(key ActionKey) (string, error) {
	var name string
	if key.Key == tcell.KeyRune {
		name = string([]rune{key.Rune})
	} else {
		var ok bool
		name, ok = tcell.KeyNames[key.Key]
		if !ok {
			return "", fmt.Errorf("Unknown key name %x", key.Key)
		}
	}

	mods := key.Modifiers
	if strings.HasPrefix(name, "Ctrl-") {
		name = name[5:]
		mods |= tcell.ModCtrl
	}
	if (mods & tcell.ModAlt) == tcell.ModAlt {
		name = "alt+" + name
	}
	if (mods & tcell.ModCtrl) == tcell.ModCtrl {
		name = "ctrl+" + name
	}
	if (mods & tcell.ModShift) == tcell.ModShift {
		name = "shift+" + name
	}
	if (mods & tcell.ModMeta) == tcell.ModMeta {
		name = "meta+" + name
	}

	return name, nil
}

func actionNameToKey(name string) (ActionKey, error) {
	key := tcell.KeyNUL
	mods := tcell.ModNone
	r := '\x00'

	bits := strings.Split(name, "+")
	for i := 0; i < (len(bits) - 1); i++ {
		mod := strings.ToLower(bits[i])
		if mod == "ctrl" {
			mods |= tcell.ModCtrl
		} else if mod == "alt" {
			mods |= tcell.ModAlt
		} else if mod == "shift" {
			mods |= tcell.ModShift
		} else if mod == "meta" {
			mods |= tcell.ModMeta
		} else {
			return ActionKey{}, fmt.Errorf("Unknown key modifier '%s'", mod)
		}
	}
	val := bits[len(bits)-1]
	log.Infof("Parse '%s'", val)
	if mods == tcell.ModCtrl {
		val = "Ctrl-" + val
		//mods = tcell.ModNone
	}
	if len(val) == 1 {
		key = tcell.KeyRune
		rs := []rune(val)
		r = rs[0]
	} else {
		found := false
		val = strings.ToLower(val)
		for kval, name := range tcell.KeyNames {
			if strings.ToLower(name) == val {
				key = kval
				found = true
				break
			}
		}
		if !found {
			return ActionKey{}, fmt.Errorf("Unknown key name '%s'", val)
		}
	}

	return ActionKey{
		Key:       key,
		Modifiers: mods,
		Rune:      r,
	}, nil
}

func (kmap *Keymap) UpdateActionMap(amap ActionMap) (bool, error) {
	log.Infof("Update actionmap %s", amap.GetContext())
	newentry := false
	for _, name := range amap.ListActions() {
		section := fmt.Sprintf("%s.%s", amap.GetContext(), name)

		entry, ok := kmap.Entries[section]
		if !ok {
			keys := amap.GetShortcuts(name)

			shortcuts := []string{}
			for _, key := range keys {
				name, err := actionKeyToName(key)
				if err != nil {
					return newentry, err
				}
				shortcuts = append(shortcuts, name)
			}

			entry := KeymapEntry{
				Context:   amap.GetContext(),
				Action:    name,
				Shortcuts: shortcuts,
			}

			kmap.Entries[section] = entry
			newentry = true
		} else {
			keys := []ActionKey{}
			for _, name := range entry.Shortcuts {
				key, err := actionNameToKey(name)
				if err != nil {
					return newentry, err
				}
				keys = append(keys, key)
				log.Infof("Convert %s -> %s", name, key.String())
			}
			log.Infof("Update %s.%s -> %d", entry.Context, entry.Action, len(keys))
			err := amap.SetShortcuts(entry.Action, keys...)
			if err != nil {
				return newentry, err
			}
		}
	}
	return newentry, nil
}

func (kmap *Keymap) LoadConfig() error {
	keymappath := xdg.ConfigPath("keymap.ini")

	log.Infof("Loading keymap config from path %s", keymappath)
	cfg, err := ini.Load(keymappath)
	if err != nil {
		return err
	}

	for _, sec := range cfg.Sections() {
		if sec.Name() == "DEFAULT" {
			continue
		}

		keybits := strings.Split(sec.Name(), ".")
		if len(keybits) != 2 {
			return fmt.Errorf("malformed keymap section name '%s'", sec.Name())
		}

		entry := KeymapEntry{
			Context: keybits[0],
			Action:  keybits[1],
		}

		for _, key := range sec.Keys() {
			if !strings.HasPrefix(key.Name(), "shortcut-") {
				return fmt.Errorf("malformed key '%s' in section '%s'", key.Name(), sec.Name())
			}

			entry.Shortcuts = append(entry.Shortcuts, key.String())
		}

		kmap.Entries[sec.Name()] = entry
	}

	return nil
}

const keynameHelp = "" +
	"Shortcut key names are either:\n" +
	" \n" +
	" * A single letter for ascii printable characters\n" +
	" * A symbolic key name for non-printable characters\n" +
	" \n" +
	"Valid symbolic names are defined in the KeyNames variable\n" +
	" \n" +
	"  https://github.com/gdamore/tcell/blob/master/key.go"

func (kmap *Keymap) SaveConfig() error {
	keymappath := xdg.ConfigPath("keymap.ini")

	cfg := ini.Empty()

	def := cfg.Section("DEFAULT")
	def.Comment = keynameHelp

	sections := []string{}
	for key, _ := range kmap.Entries {
		sections = append(sections, key)
	}
	sort.Strings(sections)
	for _, key := range sections {
		entry, _ := kmap.Entries[key]
		sec, _ := cfg.NewSection(key)
		for i := 0; i < len(entry.Shortcuts); i++ {
			sec.NewKey(fmt.Sprintf("shortcut-%d", i), entry.Shortcuts[i])
		}
	}

	log.Infof("Saving keymap config to path %s", keymappath)
	cfgdir := filepath.Dir(keymappath)
	err := os.MkdirAll(cfgdir, 0700)
	if err != nil {
		return err
	}

	err = cfg.SaveTo(keymappath)
	if err != nil {
		return err
	}

	return nil
}
