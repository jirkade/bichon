// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2021 Red Hat, Inc.

package view

import (
	"fmt"
	"strings"

	"github.com/gdamore/tcell/v2"
	log "github.com/sirupsen/logrus"
)

type ActionImpl func() bool

type ActionKey struct {
	Key       tcell.Key
	Modifiers tcell.ModMask
	Rune      rune
}

type ActionMap interface {
	RegisterAction(name, title string, impl ActionImpl, shortcuts ...ActionKey)
	SetShortcuts(name string, shortcut ...ActionKey) error
	InvokeAction(name string) bool
	HandleInput(event *tcell.EventKey) *tcell.EventKey
	FormatSummary(name ...string) string
	GetContext() string
	ListActions() []string
	GetShortcuts(name string) []ActionKey
}

func NewActionKey(key tcell.Key, mods tcell.ModMask) ActionKey {
	return ActionKey{
		Key:       key,
		Modifiers: mods,
		Rune:      '\x00',
	}
}

func NewActionRune(r rune, mods tcell.ModMask) ActionKey {
	return ActionKey{
		Key:       tcell.KeyRune,
		Modifiers: mods,
		Rune:      r,
	}
}

func (key *ActionKey) String() string {
	var keyname string
	if key.Key == tcell.KeyRune {
		keyname = fmt.Sprintf("%c", key.Rune)
	} else {
		keyname = tcell.KeyNames[key.Key]
	}
	var mods []string
	if key.Modifiers&tcell.ModShift != 0 {
		mods = append(mods, "Shift+")
	}
	if key.Modifiers&tcell.ModAlt != 0 {
		mods = append(mods, "Alt+")
	}
	if key.Modifiers&tcell.ModMeta != 0 {
		mods = append(mods, "Meta+")
	}
	if key.Modifiers&tcell.ModCtrl != 0 {
		mods = append(mods, "Ctrl+")
		if strings.HasPrefix(keyname, "Ctrl-") {
			keyname = keyname[5:]
		}
	}
	return strings.Join(mods, "") + keyname
}

func (key *ActionKey) Matches(event *tcell.EventKey) bool {
	if key.Key != event.Key() {
		return false
	}
	if key.Modifiers != event.Modifiers() {
		return false
	}
	if key.Key == tcell.KeyRune {
		return key.Rune == event.Rune()
	}

	return true
}

type ActionEntry struct {
	Title     string
	Impl      ActionImpl
	Shortcuts []ActionKey
}

type ActionHandler struct {
	Context string
	Actions map[string]ActionEntry
	Filter  func(event *tcell.EventKey) bool
}

func NewActionHandler(context string, filter func(event *tcell.EventKey) bool) ActionMap {
	return &ActionHandler{
		Context: context,
		Actions: make(map[string]ActionEntry),
		Filter:  filter,
	}
}

func (actmap *ActionHandler) GetContext() string {
	return actmap.Context
}

func (actmap *ActionHandler) RegisterAction(name, title string, impl ActionImpl, shortcuts ...ActionKey) {
	actmap.Actions[name] = ActionEntry{
		Title:     title,
		Impl:      impl,
		Shortcuts: shortcuts,
	}
}

func (actmap *ActionHandler) SetShortcuts(name string, shortcuts ...ActionKey) error {
	entry, ok := actmap.Actions[name]
	if ok {
		entry.Shortcuts = shortcuts
		actmap.Actions[name] = entry
		l := []string{}
		for _, s := range shortcuts {
			l = append(l, s.String())
		}
		log.Infof("Set shortcut %s.%s -> %s",
			actmap.Context, name,
			strings.Join(l, ","))
		return nil
	}

	return fmt.Errorf("No action '%s'", name)
}

func (actmap *ActionHandler) InvokeAction(name string) bool {
	entry, ok := actmap.Actions[name]
	if !ok {
		return false
	}

	entry.Impl()

	return true
}

func (actmap *ActionHandler) HandleInput(event *tcell.EventKey) *tcell.EventKey {
	log.Infof("Input  key=%x mods=%x name=%s rune=%c",
		event.Key(), event.Modifiers(), event.Name(), event.Rune())

	if actmap.Filter != nil && actmap.Filter(event) {
		return event
	}

	for _, entry := range actmap.Actions {
		for _, shortcut := range entry.Shortcuts {
			if shortcut.Matches(event) {
				consumed := entry.Impl()
				if consumed {
					return nil
				}
				return event
			}
		}
	}

	return event
}

func (actmap *ActionHandler) ListActions() []string {
	names := []string{}
	for key, _ := range actmap.Actions {
		names = append(names, key)
	}

	return names
}

func (actmap *ActionHandler) GetShortcuts(name string) []ActionKey {
	act, ok := actmap.Actions[name]
	if !ok {
		return []ActionKey{}
	}

	return act.Shortcuts
}

func (actmap *ActionHandler) FormatSummary(names ...string) string {
	var help []string
	for _, name := range names {
		action, ok := actmap.Actions[name]
		if !ok {
			return fmt.Sprintf("action '%s' is not registered", name)
		}
		if len(action.Shortcuts) == 0 {
			continue
		}
		key := action.Shortcuts[0]

		help = append(help, fmt.Sprintf("%s:%s", key.String(), action.Title))
	}

	return strings.Join(help, " ")
}
