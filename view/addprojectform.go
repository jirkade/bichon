// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package view

import (
	log "github.com/sirupsen/logrus"

	"gitlab.com/bichon-project/bichon/model"
	"gitlab.com/bichon-project/tview"
)

type AddProjectFormListener interface {
	AddProjectFormCancel()
	AddProjectFormConfirm(repo model.Repo)
	AddProjectFormAutoFillToken(server, project string) string
}

type AddProjectForm struct {
	tview.Primitive

	Form        *tview.Form
	Application *tview.Application
	Listener    AddProjectFormListener
	Directory   *tview.InputField
	Remote      *tview.InputField
	NickName    *tview.InputField
	Server      *tview.InputField
	Project     *tview.InputField
	Token       *tview.InputField
	Global      *tview.Checkbox
}

func NewAddProjectForm(app *tview.Application, listener AddProjectFormListener) *AddProjectForm {
	form := &AddProjectForm{
		Form:        tview.NewForm(),
		Application: app,
		Listener:    listener,
	}

	form.Primitive = Modal(form.Form, 60, 22)

	form.Directory = addInputField(form.Form, "Directory", "", 40, false)
	form.Remote = addInputField(form.Form, "Remote", "origin", 20, false)
	form.NickName = addInputField(form.Form, "Nick name", "", 40, false)
	form.Server = addInputField(form.Form, "Server", "", 20, false)
	form.Project = addInputField(form.Form, "Project", "", 20, false)
	form.Token = addInputField(form.Form, "API Token", "", 40, false)
	mask := addCheckbox(form.Form, "", "Show API token", false, false)
	form.Global = addCheckbox(form.Form, "Global", "Use token for all repos on this server", true, false)

	// XXX use a unicode mask but InputField is broken
	// wrt measuring character size
	form.Token.SetMaskCharacter('*')

	mask.SetChangedFunc(func(checked bool) {
		if checked {
			form.Token.SetMaskCharacter(rune(0))
		} else {
			form.Token.SetMaskCharacter('*')
		}
	})

	form.Form.SetBorder(true).
		SetTitle("Add a new project")

	form.Form.AddDefaultButton("Save", form.confirmButton)
	form.Form.AddButton("Cancel", form.cancelButton)
	form.Form.SetCancelFunc(form.cancelButton)

	form.Directory.SetChangedFunc(form.autoCompleteRepo)
	form.Remote.SetChangedFunc(form.autoCompleteRepo)

	form.Project.SetChangedFunc(form.autoCompleteToken)

	return form
}

func (form *AddProjectForm) GetName() string {
	return "add-project-form"
}

func (form *AddProjectForm) autoCompleteRepo(text string) {
	dir := form.Directory.GetText()
	remote := form.Remote.GetText()

	log.Infof("Auto complete repo for '%s' & '%s'", dir, remote)

	if dir == "" || remote == "" {
		return
	}

	repo, err := model.NewRepoForDirectory(dir, remote)
	if err != nil {
		return
	}

	form.NickName.SetText(repo.NickName)
	form.Server.SetText(repo.Server)
	form.Project.SetText(repo.Project)
}

func (form *AddProjectForm) autoCompleteToken(text string) {
	server := form.Server.GetText()
	project := form.Project.GetText()

	log.Infof("Auto complete token for '%s' & '%s'", server, project)

	token := form.Listener.AddProjectFormAutoFillToken(server, project)
	form.Token.SetText(token)
}

func (form *AddProjectForm) clearText() {
	form.Directory.SetText("")
	form.Remote.SetText("origin")
	form.NickName.SetText("")
	form.Server.SetText("")
	form.Project.SetText("")
	form.Token.SetText("")
	form.Global.SetChecked(true)

	form.Form.SetFocus(0)
	form.Application.SetFocus(form.Form)
}

func (form *AddProjectForm) confirmButton() {
	dir := form.Directory.GetText()
	remote := form.Remote.GetText()
	nickname := form.NickName.GetText()
	server := form.Server.GetText()
	proj := form.Project.GetText()
	token := form.Token.GetText()
	global := form.Global.IsChecked()

	if len(proj) > 0 && proj[0] == '/' {
		proj = proj[1:]
	}

	if len(proj) > 0 && proj[len(proj)-1] == '/' {
		proj = proj[:len(proj)-1]
	}

	repo := model.NewRepo(nickname, dir, remote, server, proj, token, global, model.RepoStateActive)

	form.clearText()
	form.Listener.AddProjectFormConfirm(*repo)
}

func (form *AddProjectForm) cancelButton() {
	form.clearText()
	form.Listener.AddProjectFormCancel()
}

func (form *AddProjectForm) LoadLocalProject() {
	gitdir, err := model.FindGitDir()

	if err == nil {
		form.Directory.SetText(gitdir)
	}
}
